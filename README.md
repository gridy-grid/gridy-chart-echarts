# GridyGrid chart component ECharts driver

## Installation

```shell
npm i gridy-grid echarts gridy-chart-echarts gridy-grid-default sk-theme-default
```

and plug it to your page

```html
<script src="/node_modules/echarts/dist/echarts.js"></script>
```

then add element to your container or setup it programmatically

```html
    <gridy-grid id="gridyGrid" base-path="/node_modules/gridy-grid/src" sort-field="$.title">
    <gridy-data-source fields='[{ "title": "Name", "path": "$.name" },{ "title": "Value", "path": "$.value"}]'
                   datasource-type="DataSourceLocal" datapath="$.data"></gridy-data-source>

    <gridy-chart id="gridyChart2" legend='{"data": ["sales"]}' type="bar" dri="echarts" field-name="Name" field-value="Value" width="400" height="400"></gridy-chart>

</gridy-grid>
```

Multiple charts support

```html
<gridy-grid id="gridyGrid2" base-path="/node_modules/gridy-grid/src" sort-field="$.title">
    <gridy-data-source fields='[{ "title": "Name", "path": "$.name" },{ "title": "Value", "path": "$.value"},{ "title": "Group", "path": "$.group"}]'
                       datasource-type="DataSourceLocal" datapath="$.data"></gridy-data-source>
	<gridy-chart id="gridyChart2" options='{"xAxis": { "data": "names" }, "yAxis": {}}' legend='{"data": ["sales", "markt"]}' type="multi:bar:sales,line:markt" value-field dri="echarts" field-color="Color" field-name="Name" field-value="Value" field-group="Group" width="400" height="400"></gridy-chart>
    <gridy-pager id="gridyPager2"></gridy-pager>
</gridy-grid>
<script type="module">
    import { GridyGrid } from '/node_modules/gridy-grid/src/gridy-grid.js';
    let data = [];
    data.push({ name: 'FooBar1', value: 100, group: 'sales' });
    data.push({ name: 'FooBar1', value: 100, group: 'markt' });
    data.push({ name: 'FooBar2', value: 230, group: 'sales' });
    data.push({ name: 'FooBar2', value: 230, group: 'markt' });
    data.push({ name: 'FooBar3', value: 340, group: 'sales' });
    data.push({ name: 'FooBar3', value: 340, group: 'markt' });
    data.push({ name: 'FooBar4', value: 170, group: 'sales' });
    data.push({ name: 'FooBar4', value: 170, group: 'markt' });
    data.push({ name: 'FooBar5', value: 570, group: 'sales' });
    data.push({ name: 'FooBar5', value: 570, group: 'markt' });
    let grid = document.querySelector('#gridyGrid');
    grid.addEventListener('bootstrap', () => {
        grid.charts[0].addEventListener('skrender', (event) => {
            grid.dataSource.loadData(data);
        });
    });
    customElements.define('gridy-grid', GridyGrid);
</script>
```

## Options

you can specify options for echarts as attribute of gridy-chart.

When xAxis or yAxis specified as string it will be populated from chart driver
context by name. Note: some chart types may require at least empty x(y)Axis in options.

```html
	<gridy-chart id="gridyChart2" options='{"xAxis": { "data": "names" }, "yAxis": {}}' type="line" dri="echarts" field-name="Name" field-value="Value" field-group="Group" inner-radius="50" width="400" height="400"></gridy-chart> 
```

JSONPath expressions on the same chart driver context are also supported for 
data fields.

### attributes

**echarts-renderer** - renderer parameter for echarts init, possible options: 'svg', 'canvas' (default: 'canvas')

**echarts-theme** - theme parameter for echarts init (default: null)

**value-field** passes data extracted from specified field to option.series.data array, if not specified item is passed 'as-is' after mapping
