import { GridyChartDriver } from "../../gridy-grid/src/chart/gridy-chart-driver.js";
import { FieldMappedChart } from "../../gridy-grid/src/chart/impl/field-mapped-chart.js";
import { SK_ATTR_CHANGE_EVT } from "../../sk-core/src/event/sk-attr-change-event.js";


export class DefaultEchartsChart extends FieldMappedChart {
    
    get isMultiChart() {
        return this.el.chartType.match(/multi.*/) !== null;
    }
    
    get multiChartTypes() {
        if (! this._multiChartTypes) {
            this._multiChartTypes = this.el.chartType.replace('multi:', '').split(',') || [];
        }
        return this._multiChartTypes;
    }
    
    get reqFields() {
		if (! this._reqFields) {
            if (this.isMultiChart) {
                this._reqFields = ['name', 'value', 'group'];
            } else {
                this._reqFields = ['name', 'value'];
            }
		}
		return this._reqFields;
	}
    
    get idxFields() {
		if (! this._idxFields) {
			this._idxFields = ['name'];
		}
		return this._idxFields;
	}
    
    get chartRenderer() {
        return this.el.hasAttribute('echarts-renderer') ? this.el.getAttribute('echarts-renderer') : 'canvas';
    }
    
    get chartTheme() {
        return this.el.hasAttribute('echarts-theme') ? this.el.getAttribute('echarts-theme') : null;
    }
    
    render(container, options) {
        let chart = echarts.init(container, this.chartTheme, { renderer: this.chartRenderer });

        
        chart.setOption(options);
        return chart;
    }
}

export class GridyChartEcharts extends GridyChartDriver {


    
    implByType(type) {				
        return new DefaultEchartsChart(this.el);
	}
    
    get titleCfg() {
        if (! this._titleCfg) {
            if (this.el.hasAttribute('title')) {
                try {
                    this._titleCfg = JSON.parse(this.el.getAttribute('title'));
                } catch (e) {
                    this._titleCfg = { text: this.el.getAttribute('title') };
                }
            } else {
                return null;
            }
        }
        return this._titleCfg;
    }
    
    set titleCfg(titleCfg) {
        this._titleCfg = titleCfg;
        this.el.setAttribute('title', JSON.stringify(titleCfg));
    }
    
    get tooltipCfg() {
        if (! this._tooltipCfg) {
            if (this.el.hasAttribute('tooltip')) {
                try {
                    this._tooltipCfg = JSON.parse(this.el.getAttribute('tooltip'));
                } catch (e) {
                    this._tooltipCfg = { text: this.el.getAttribute('tooltip') };
                }
            } else {
                return null;
            }
        }
        return this._tooltipCfg;
    }
    
    set tooltipCfg(tooltipCfg) {
        this._tooltipCfg = tooltipCfg;
        this.el.setAttribute('tooltip', JSON.stringify(tooltipCfg));
    }
    
    get legendCfg() {
        if (! this._legendCfg) {
            if (this.el.hasAttribute('legend')) {
                try {
                    this._legendCfg = JSON.parse(this.el.getAttribute('legend'));
                } catch (e) {
                    this._legendCfg = { text: this.el.getAttribute('legend') };
                }
            } else {
                return null;
            }
        }
        return this._legendCfg;
    }
    
    set legendCfg(legendCfg) {
        this._legendCfg = legendCfg;
        this.el.setAttribute('legend', JSON.stringify(legendCfg));
    }
    
    async setupOptions(el) {
        let options = {
            //xAxis: {},
            //yAxis: {},
        };
        if (el.options) {
            Object.assign(options, el.options);
        }
        if (this.titleCfg) {
            options.title = this.titleCfg; 
        }
        if (this.tooltipCfg) {
            options.toooltip = this.tooltipCfg; 
        }
        if (this.legendCfg) {
            options.legend = this.legendCfg; 
        }
        let xData = await this.el.loadDataLink(options.xAxis.data, this);
        if (xData) {
            options.xAxis.data = xData;
        }
        let yData = await this.el.loadDataLink(options.yAxis.data, this);
        if (yData) {
            options.yAxis.data = yData;
        }
        return options;
    }
    
    extractValue(el, item) {
        let fieldKey = el.getAttribute('value-field') !== "" && el.getAttribute('value-field') !== "value-field" 
            ? this.el.getAttribute('value-field') : "value";
        return item[fieldKey];
    }
    
    async setupSeries(el, fmtedData, options) {
        let series = options.series ? options.series : [];
        if (this.chartImpl.isMultiChart && this.chartImpl.multiChartTypes.length > 0) {
            let groupedValues = new Map();
            for (let item of fmtedData) {
                if (item.group && ! groupedValues.get(item.group)) {
                    groupedValues.set(item.group, []);
                }
                groupedValues.get(item.group).push(el.hasAttribute('value-field') ? this.extractValue(el, item) : item);
            }
            for (let chartTypeMapping of this.chartImpl.multiChartTypes) {
                let [chartType, groupName] = chartTypeMapping.split(':');
                series.push({
                    name: groupName,
                    type: chartType,
                    data: groupedValues.get(groupName)
                });
            }
        } else {
            if (series[0] && series[0].data) {
                let seriesData = await this.el.loadDataLink(series[0].data, this);
                if (seriesData) {
                    series[0].data = seriesData;
                }
            } else {
                let values = el.hasAttribute('value-field')
                    ? fmtedData.map((item) => this.extractValue(el, item))
                    : fmtedData;
                if (series[0]) {
                    Object.assign(series[0], {
                        name: 'value',
                        type: el.chartType,
                        data: values
                    });
                } else {
                    series.push({
                        name: 'value',
                        type: el.chartType,
                        data: values
                    });
                }
            }
        }
        return series;
    }

    async renderChart(el, data) {

        this.type = el.getAttribute('type');

        this.chartImpl = this.implByType(this.type);
        this.chartImpl.configFromEl(el);

        let fmtedData = await this.chartImpl.fmtData(data);

        let container = el.querySelector('.gridy-chart-container');
        container.innerHTML = '';
        container.style.width = (el.width).toString() + "px";
        container.style.height = (el.height).toString() + "px";

        if (data && data.length > 0) {
            this.names = Array.from(this.chartImpl.byField.name.keys());
        }

        let options = await this.setupOptions(el);
        let dataSeries = await this.setupSeries(el, fmtedData, options);
        if (options.series && Array.isArray(options.series)) { // merge data to option.series items
            options.series.forEach((item, i) => {
                if (dataSeries[i]) {
                    Object.assign(item, dataSeries[i]);
                }
            });
            if (options.series.length < dataSeries.length) { // if data declares more sets than in attr
                options.series.push(...dataSeries.slice(options.series.length + 1, (dataSeries.length - options.series.length) + 1))
            }
        } else {
            options.series = dataSeries;
        }
        this.chart = this.chartImpl.render(container, options);
        this.bindOptionsChange();
        return Promise.resolve(this.chart);
    }

    bindOptionsChange() {
        if (this.optionsChangeHandler) {
            this.el.removeEventListener(SK_ATTR_CHANGE_EVT, this.optionsChangeHandler);
        }
        this.optionsChangeHandler = async function(event) {
            if (event.detail.name === 'options') {
                this.el.logger.debug('options attr changed, set it to echarts chart');
                let options = await this.setupOptions(this.el);
                this.chart.setOption(options);
            }
        }.bind(this);
        this.el.addEventListener(SK_ATTR_CHANGE_EVT, this.optionsChangeHandler);
    }
}
